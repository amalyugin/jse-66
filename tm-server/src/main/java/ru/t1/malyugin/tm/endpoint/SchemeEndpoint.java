package ru.t1.malyugin.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.malyugin.tm.api.endpoint.ISchemeEndpoint;
import ru.t1.malyugin.tm.api.service.ISchemeService;
import ru.t1.malyugin.tm.dto.request.scheme.SchemeDropRequest;
import ru.t1.malyugin.tm.dto.request.scheme.SchemeUpdateRequest;
import ru.t1.malyugin.tm.dto.response.scheme.SchemeDropResponse;
import ru.t1.malyugin.tm.dto.response.scheme.SchemeUpdateResponse;
import ru.t1.malyugin.tm.exception.server.EndpointException;

import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.malyugin.tm.api.endpoint.ISchemeEndpoint")
public final class SchemeEndpoint extends AbstractEndpoint implements ISchemeEndpoint {

    @NotNull
    @Autowired
    private ISchemeService schemeService;

    @NotNull
    @Override
    public SchemeDropResponse dropScheme(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final SchemeDropRequest request
    ) {
        @Nullable final String initToken = request.getInitToken();
        try {
            schemeService.dropScheme(initToken);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new SchemeDropResponse();
    }

    @NotNull
    @Override
    public SchemeUpdateResponse updateScheme(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final SchemeUpdateRequest request
    ) {
        @Nullable final String initToken = request.getInitToken();
        try {
            schemeService.updateScheme(initToken);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new SchemeUpdateResponse();
    }

}