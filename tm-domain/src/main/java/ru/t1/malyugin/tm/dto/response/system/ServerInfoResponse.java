package ru.t1.malyugin.tm.dto.response.system;


import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.response.AbstractResponse;


@Getter
@Setter
public final class ServerInfoResponse extends AbstractResponse {

    @Nullable
    private Integer processorsCount;

    @Nullable
    private String maxMemory;

    @Nullable
    private String totalMemory;

    @Nullable
    private String freeMemory;

    @Nullable
    private String usedMemory;

}