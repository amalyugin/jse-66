package ru.t1.malyugin.tm.model;


import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractUserOwnedModel extends AbstractModel {

    @NotNull
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

}