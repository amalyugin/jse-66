package ru.t1.malyugin.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.dto.request.task.TaskUnbindFromProjectRequest;
import ru.t1.malyugin.tm.event.ConsoleEvent;
import ru.t1.malyugin.tm.util.TerminalUtil;

@Component
public final class TaskUnbindFromProjectListener extends AbstractTaskListener {

    @NotNull
    private static final String NAME = "task-unbind-from-project";

    @NotNull
    private static final String DESCRIPTION = "Unbind Task from Project";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskUnbindFromProjectListener.getName() == #event.message")
    public void handleConsoleEvent(@NotNull final ConsoleEvent event) {
        System.out.println("[UNBIND TASK FROM PROJECT]");

        System.out.print("ENTER PROJECT ID: ");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.print("ENTER TASK ID: ");
        @NotNull final String taskId = TerminalUtil.nextLine();

        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(getToken(), taskId, projectId);
        taskEndpoint.unbindTaskFromProject(request);
    }

}