package ru.t1.malyugin.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.malyugin.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String EMPTY_VALUE = "---";

    @Value("#{environment['application.name']}")
    private String applicationName;

    @Value("#{environment['application.log']}")
    private String applicationLog;

    @Value("#{environment['server.port']}")
    private String serverPort;

    @Value("#{environment['server.host']}")
    private String serverHost;

    @Value("#{environment['admin.login']}")
    private String adminLogin;

    @Value("#{environment['admin.pass']}")
    private String adminPass;

    @Value("#{environment['soap_user.login']}")
    private String soapLogin;

    @Value("#{environment['soap_user.pass']}")
    private String soapPass;

    @NotNull
    @Override
    public String getApplicationVersion() {
        return readManifest(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return readManifest(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return readManifest(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    private String readManifest(@Nullable final String key) {
        if (StringUtils.isBlank(key)) return EMPTY_VALUE;
        if (!Manifests.exists(key)) return EMPTY_VALUE;
        return Manifests.read(key);
    }

}