package ru.t1.malyugin.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.dto.model.TaskDTO;
import ru.t1.malyugin.tm.dto.request.task.TaskShowListRequest;
import ru.t1.malyugin.tm.enumerated.EntitySort;
import ru.t1.malyugin.tm.event.ConsoleEvent;
import ru.t1.malyugin.tm.util.TerminalUtil;

import java.util.List;

@Component
public final class TaskListShowListener extends AbstractTaskListener {

    @NotNull
    private static final String NAME = "task-list";

    @NotNull
    private static final String DESCRIPTION = "Show task list";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskListShowListener.getName() == #event.message")
    public void handleConsoleEvent(@NotNull final ConsoleEvent event) {
        System.out.println("[SHOW TASK LIST]");

        System.out.print("ENTER SORT: ");
        System.out.print(EntitySort.renderValuesList());
        @Nullable final Integer sortIndex = TerminalUtil.nextIntegerSafe();
        @Nullable final EntitySort sort = EntitySort.getSortByIndex(sortIndex);

        @NotNull final TaskShowListRequest request = new TaskShowListRequest(getToken(), sort);
        @Nullable final List<TaskDTO> tasks = taskEndpoint.showTaskList(request).getTaskList();
        renderTaskList(tasks);
    }

}