package ru.t1.malyugin.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.exception.AbstractException;

public abstract class AbstractSystemException extends AbstractException {

    protected AbstractSystemException() {
    }

    protected AbstractSystemException(@NotNull final String message) {
        super(message);
    }

    protected AbstractSystemException(@NotNull final String message, @NotNull Throwable cause) {
        super(message, cause);
    }

    protected AbstractSystemException(@NotNull final Throwable cause) {
        super(cause);
    }

    protected AbstractSystemException(@NotNull final String message, @NotNull final Throwable cause,
                                      final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}