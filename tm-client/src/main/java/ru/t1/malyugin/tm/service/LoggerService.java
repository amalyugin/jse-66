package ru.t1.malyugin.tm.service;

import lombok.Cleanup;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.malyugin.tm.api.service.ILoggerService;
import ru.t1.malyugin.tm.api.service.IPropertyService;

import javax.annotation.PostConstruct;
import javax.xml.ws.soap.SOAPFaultException;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.*;

@Service
public final class LoggerService implements ILoggerService {

    @NotNull
    private static final String CONFIG_FILE_LOCATION = "/logger.properties";

    @NotNull
    private static final String COMMANDS = "COMMANDS";

    @NotNull
    private static final String COMMANDS_FILE = "commands_log.xml";

    @NotNull
    private static final String ERRORS = "ERRORS";

    @NotNull
    private static final String ERRORS_FILE = "errors_log.xml";

    @NotNull
    private static final String MESSAGES = "MESSAGES";

    @NotNull
    private static final String MESSAGES_FILE = "messages_log.xml";

    @NotNull
    private static final LogManager LOG_MANAGER = LogManager.getLogManager();

    @NotNull
    private static final Logger LOGGER_ROOT = Logger.getLogger("");

    @NotNull
    private static final Logger LOGGER_COMMAND = Logger.getLogger(COMMANDS);

    @NotNull
    private static final Logger LOGGER_ERROR = Logger.getLogger(ERRORS);

    @NotNull
    private static final Logger LOGGER_MESSAGE = Logger.getLogger(MESSAGES);

    @NotNull
    private static final ConsoleHandler CONSOLE_HANDLER = getConsoleHandler();

    @NotNull
    private final IPropertyService propertyService;

    @Autowired
    public LoggerService(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
    }

    private static void loadConfigFromFile() {
        try {
            @NotNull final Class<?> clazz = LoggerService.class;
            @Cleanup @Nullable final InputStream inputStream = clazz.getResourceAsStream(CONFIG_FILE_LOCATION);
            LOG_MANAGER.readConfiguration(inputStream);
        } catch (@NotNull final IOException e) {
            LOGGER_ROOT.severe(e.getMessage());
        }
    }

    @NotNull
    private static ConsoleHandler getConsoleHandler() {
        @NotNull final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(final LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    private static void registry(
            @NotNull final Logger logger,
            @Nullable final String fileName,
            final boolean isConsole
    ) {
        try {
            if (isConsole) logger.addHandler(CONSOLE_HANDLER);
            logger.setUseParentHandlers(false);
            if (!StringUtils.isBlank(fileName)) logger.addHandler(new FileHandler(fileName));
        } catch (@NotNull final IOException e) {
            LOGGER_ROOT.severe(e.getMessage());
        }
    }

    @PostConstruct
    private void initLogger() {
        loadConfigFromFile();
        @NotNull final String logDir = propertyService.getApplicationLog();
        registry(LOGGER_COMMAND, logDir + COMMANDS_FILE, false);
        registry(LOGGER_ERROR, logDir + ERRORS_FILE, true);
        registry(LOGGER_MESSAGE, logDir + MESSAGES_FILE, true);
    }

    @Override
    public void info(@Nullable final String message) {
        if (StringUtils.isBlank(message)) return;
        LOGGER_MESSAGE.info(message);
    }

    @Override
    public void command(@Nullable final String message) {
        if (StringUtils.isBlank(message)) return;
        LOGGER_COMMAND.info(message);
    }

    @Override
    public void debug(@Nullable final String message) {
        if (StringUtils.isBlank(message)) return;
        LOGGER_MESSAGE.fine(message);
    }

    @Override
    public void error(@Nullable final Exception e) {
        if (e == null) return;
        LOGGER_ERROR.log(Level.SEVERE, e.getMessage(), e);
    }

    @Override
    public void errorSOAP(@Nullable SOAPFaultException e) {
        if (e == null) return;
        LOGGER_ERROR.log(Level.SEVERE, e.getFault().getFaultString(), e);
    }

}