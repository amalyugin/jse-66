package ru.t1.malyugin.tm.controller;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.malyugin.tm.client.TaskRestEndpointClient;
import ru.t1.malyugin.tm.client.TasksRestEndpointClient;
import ru.t1.malyugin.tm.marker.IntegrationCategory;
import ru.t1.malyugin.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

@Category(IntegrationCategory.class)
public class TasksTest {

    private static final TasksRestEndpointClient TASKS_CLIENT = TasksRestEndpointClient.getInstance();
    private static final TaskRestEndpointClient TASK_CLIENT = TaskRestEndpointClient.getInstance();
    private final List<Task> tasks = new ArrayList<>();
    private final Task task1 = new Task("T1", "D1");
    private final Task task2 = new Task("T2", "D2");

    @Before
    public void before() {
        tasks.add(task1);
        tasks.add(task2);
        TASK_CLIENT.post(task1);
        TASK_CLIENT.post(task2);
    }

    @After
    public void after() {
        tasks.clear();
        TASKS_CLIENT.clear();
    }

    @Test
    public void getTest() {
        final Iterable<Task> taskList = TASKS_CLIENT.get();
        Assert.assertNotNull(taskList);
    }

    @Test
    public void countTest() {
        Assert.assertEquals(tasks.size(), TASKS_CLIENT.count());
        final Task task = new Task("NEW", "NEW");
        TASK_CLIENT.post(task);
        Assert.assertEquals(tasks.size() + 1, TASKS_CLIENT.count());
    }

    @Test
    public void deleteAllTest() {
        long count = TASKS_CLIENT.count();
        final Task p1 = new Task("1", "1");
        final Task p2 = new Task("2", "2");
        final Task p3 = new Task("3", "3");
        final List<Task> toAdd = new ArrayList<>();
        toAdd.add(p1);
        toAdd.add(p2);
        toAdd.add(p3);
        TASK_CLIENT.post(p1);
        TASK_CLIENT.post(p2);
        TASK_CLIENT.post(p3);
        count = count + 3;
        Assert.assertEquals(count, TASKS_CLIENT.count());
        TASKS_CLIENT.delete(toAdd);
        count = count - 3;
        Assert.assertEquals(count, TASKS_CLIENT.count());
    }

}