<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en" xml:lang="en">
<head>
    <title>TASK MANAGER</title>
</head>
<style>
    *,
    *:before,
    *:after {
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
    }

    form {
        max-width: 500px;
        margin: 10px auto;
        padding: 10px 10px;
        background: #f4f7f8;
    }

    input[type="text"],
    input[type="password"],
    input[type="date"],
    input[type="datetime"],
    input[type="email"],
    input[type="number"],
    input[type="search"],
    input[type="tel"],
    input[type="time"],
    input[type="url"],
    textarea,
    select {
        background: rgba(255, 255, 255, 0.1);
        border: none;
        font-size: 16px;
        height: auto;
        margin: 0;
        outline: 0;
        padding: 10px;
        width: 100%;
        background-color: #e8eeef;
        color: #8a97a0;
        margin-bottom: 10px;
    }

    fieldset {
        margin-bottom: 30px;
        border: none;
    }

    label {
        display: block;
        margin-bottom: 8px;
    }

    button {
        background-color: #ededed;
        border-width: 0;
        color: #333333;
        cursor: pointer;
        display: inline-block;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 14px;
        font-weight: 500;
        line-height: 20px;
        list-style: none;
        margin: 0;
        padding: 10px 12px;
        text-align: center;
        transition: all 200ms;
        vertical-align: baseline;
        white-space: nowrap;
        user-select: none;
        -webkit-user-select: none;
        touch-action: manipulation;
    }

    h1 {
        font-family: Arial, Helvetica, sans-serif;
        font-size: 16px;
    }

    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #ededed;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #ededed;
    }

    ul.menu {
        margin: 0;
        padding: 0;
        list-style: none;
        height: 36px;
        line-height: 36px;
        background: #dddddd;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13px;
    }

    ul.menu li {
        border-right: 1px solid #ededed;
        float: left;
    }

    ul.menu a {
        display: block;
        padding: 0 28px;
        color: #0c0909;
        text-decoration: none;
    }

    ul.menu a:hover,
    ul.menu li.current a {
        background: #ededed;
    }


</style>
<body>

<ul class="menu">
    <li><a href="/">NAIN</a></li>
    <li><a href="/projects">PROJECTS</a></li>
    <li><a href="/tasks">TASKS</a></li>
</ul>
<br/>