<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>
<table border="1">
    <caption>TASK LIST</caption>
    <tr>
        <th scope="col">ID</th>
        <th scope="col">NAME</th>
        <th scope="col">DESCRIPTION</th>
        <th scope="col">STATUS</th>
        <th scope="col">PROJECT_ID</th>
        <th scope="col">START</th>
        <th scope="col">FINISH</th>
        <th scope="col">EDIT</th>
        <th scope="col">DELETE</th>
    </tr>
    <c:forEach var="task" items="${tasks}">
        <tr>
            <td><c:out value="${task.id}"/></td>
            <td><c:out value="${task.name}"/></td>
            <td><c:out value="${task.description}"/></td>
            <td><c:out value="${task.status.displayName}"/></td>
            <td><c:out value="${task.projectId}"/></td>
            <td><fmt:formatDate pattern="dd.MM.yyyy" value="${task.start}"/></td>
            <td><fmt:formatDate pattern="dd.MM.yyyy" value="${task.finish}"/></td>
            <td><a href="/task/edit/${task.id}"/>EDIT</td>
            <td><a href="/task/delete/${task.id}"/>DELETE</td>
        </tr>
    </c:forEach>
</table>
<br/>
<form action="/task/create">
    <button>CREATE TASK</button>
</form>

<jsp:include page="../include/_footer.jsp"/>