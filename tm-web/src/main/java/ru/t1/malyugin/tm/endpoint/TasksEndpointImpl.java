package ru.t1.malyugin.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.malyugin.tm.api.endpoint.TasksRestEndpoint;
import ru.t1.malyugin.tm.api.service.ITaskService;
import ru.t1.malyugin.tm.model.Task;

@RestController
@RequestMapping("/api/tasks")
public class TasksEndpointImpl implements TasksRestEndpoint {

    @Autowired
    private ITaskService taskService;


    @Override
    @GetMapping("/get")
    public Iterable<Task> get() {
        return taskService.findAll();
    }

    @Override
    @GetMapping("/count")
    public int count() {
        return (int) taskService.count();
    }

    @Override
    @DeleteMapping("/delete")
    public void delete(@RequestBody Iterable<Task> tasks) {
        taskService.deleteAll(tasks);
    }

    @Override
    @DeleteMapping("/delete/all")
    public void clear() {
        taskService.clear();
    }


}