package ru.t1.malyugin.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.malyugin.tm.api.endpoint.TaskRestEndpoint;
import ru.t1.malyugin.tm.api.service.ITaskService;
import ru.t1.malyugin.tm.model.Task;

@RestController
@RequestMapping("/api/task")
public class TaskEndpointImpl implements TaskRestEndpoint {

    @Autowired
    private ITaskService taskService;

    @Override
    @GetMapping("/get/{id}")
    public Task get(@PathVariable("id") String id) {
        return taskService.findById(id);
    }

    @Override
    @PostMapping("/post")
    public void post(@RequestBody Task task) {
        taskService.add(task);
    }

    @Override
    @PutMapping("/put")
    public void put(@RequestBody Task task) {
        taskService.edit(task);
    }

    @Override
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") String id) {
        taskService.deleteById(id);
    }

}