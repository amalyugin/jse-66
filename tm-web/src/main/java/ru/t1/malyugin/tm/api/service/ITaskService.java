package ru.t1.malyugin.tm.api.service;

import ru.t1.malyugin.tm.model.Task;

public interface ITaskService {

    Iterable<Task> findAll();

    long count();

    void create();

    void add(Task task);

    void deleteById(String id);

    void deleteAll(Iterable<Task> tasks);

    void clear();

    void edit(Task task);

    Task findById(String id);

}