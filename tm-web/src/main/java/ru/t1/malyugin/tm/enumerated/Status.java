package ru.t1.malyugin.tm.enumerated;

import lombok.Getter;

public enum Status {

    NOT_STARTED("Not Started"), IN_PROGRESS("In Progress"), COMPLETED("Completed");

    @Getter
    private final String displayName;

    Status(final String displayName) {
        this.displayName = displayName;
    }

}