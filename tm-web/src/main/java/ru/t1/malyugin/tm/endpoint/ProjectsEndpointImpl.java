package ru.t1.malyugin.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.malyugin.tm.api.endpoint.ProjectsRestEndpoint;
import ru.t1.malyugin.tm.api.service.IProjectService;
import ru.t1.malyugin.tm.model.Project;

@RestController
@RequestMapping("/api/projects")
public class ProjectsEndpointImpl implements ProjectsRestEndpoint {

    @Autowired
    private IProjectService projectService;

    @Override
    @GetMapping("/get")
    public Iterable<Project> get() {
        return projectService.findAll();
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return (int) projectService.count();
    }

    @Override
    @DeleteMapping("/delete")
    public void delete(@RequestBody Iterable<Project> projects) {
        projectService.deleteAll(projects);
    }

    @Override
    @DeleteMapping("/delete/all")
    public void clear() {
        projectService.clear();
    }

}