package ru.t1.malyugin.tm.configuration;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.t1.malyugin.tm")
public class ApplicationConfiguration {

}